from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger

from apps.bookssimilarities.models import BooksSimilarities

if __name__ == '__main__':
    scheduler = BackgroundScheduler()
    scheduler.add_job(BooksSimilarities.update_all_similarities, CronTrigger.from_crontab('0 3 1 * *'))
    scheduler.start()
