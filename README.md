# **TCC Livros API**
[![pipeline status](https://gitlab.com/tcc-livros/tcc-livros-api/badges/develop/pipeline.svg)](https://gitlab.com/tcc-livros/tcc-livros-api/-/commits/develop)
[![coverage report](https://gitlab.com/tcc-livros/tcc-livros-api/badges/develop/coverage.svg)](https://gitlab.com/tcc-livros/tcc-livros-api/-/commits/develop)
  

Projeto utiliza [Django 3.0.2](https://docs.djangoproject.com/en/3.0/) e [django rest framework](https://www.django-rest-framework.org)

  
  

## Clonar projeto para desenvolvimento

User o comando para clonar o projeto em seu computador

```bash

$ https://gitlab.com/tcc-livros/tcc-livros-api.git

```

## Instalar dependências

### Ambiente virtual

Vá para o diretório do projeto

```

$ cd tcc-livros-api

```

Para instalar o ambiente virtual use os comandos

```

$ pip install virtualenv

$ virtualenv venv -p python

```

Para ativar o ambiente virtual use o comando

- linux `source venv/bin/activate`

- windows `venv\Scripts\activate`

### Instalação das dependências

Para instalar as dependências use o comando

```

$ pip install -r requirements-dev.txt

```

  

## HEROKU
### Para utilizar Heroku localmente:
* Instale [heroku-cli](https://devcenter.heroku.com/articles/heroku-cli)
* Faça login  
``` $ heroku login ```
* Para criar um superuser user o comando:
``` $ heroku run python3 manage.py createsuperuser```


### API
* Para acessar a página admin (depois de ter criado o superuser) utilize:
 ```https://tcc-livros.herokuapp.com/admin```

* Url Base
```https://tcc-livros.herokuapp.com/api/```
* Endpoint _user_
	* GET todos usuários
	 
	```accounts/users/```
	
	* POST criar novo usuário (campos obrigatórios: _email_, _password_)
	
	```accounts/register/```

    * POST login(token do usuário) (campos obrigatórios: _email_, _password_)
	
	```accounts/login/```
	
	* GET pesquisa por usuário (campos de pesquisa: _name_, _email_)
	
	```accounts/users/?search=```
	
* Endpoint _club_
    * GET todos clubes
    
    ```api/bookclubs/```
    
    * POST novo clube (campos obrigatórios: _name_, _telegram_, _description_, _theme_)
    
    ```bookclubs/```
    
    * GET pesquisar por club (campos de pesquisa: _name_, _theme_)
    
    ```bookclubs/?search=```
    
* Endpoint _books_
    * GET todos os livros

    ```books/```
    
    * GET pesquisar por livro (campos de pesquisa: _title_)
    ```books/?search=```
   
* Endpoint _bookshelf_
    * GET todos os _bookshelf_

    ```bookshelfs/```

    * GET pesquisar por _bookshelf_ (campos de pesquisa: _available_, _owner_id_, _book_title_)
  
    ```bookshelfs/?search=```
    
    * POST novo livro a estante do usuário (campos obrigatórios: ISBN)
    
    ```bookshelfs/```
    
* Endpoint _shared-bookshelf_
    * POST novo empréstimo de livro (solicitado) (campos obrigatório: bookshelf [id] )
    
    ```shared-bookshelf/```
    
    * ACTIONS 
        * PATCH
            * `shared-bookshelf/[shared_bookshelf_id]/borrow/` `shared_bookshelf.solicitor` confirma que o empréstimo lhe foi realizado
            * `shared-bookshelf/[shared_bookshelf_id]/return/` `shared_bookshelf.bookshelf.owner` confirma que o empréstimo lhe foi retornado
            * `shared-bookshelf/[shared_bookshelf_id]/denie/` `shared_bookshelf.bookshelf.owner` confirma que o empréstimo lhe foi negado

    * GET
        * Lista de livros emprestados
        
        ```shared-bookshelf/borrowed/```   
        
        * Lista de livros solicitados
        
        ```shared-bookshelf/solicited/```

    * PUT, PATCH e DELETE não é permitido
    
* Endpoint _recomendations_
    * GET top 5 recomendações do usuário (o usuário precisa estar autenticado com token)
    
    ```recommendations/```
    
