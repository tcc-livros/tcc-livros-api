"""tcclivrosapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers

from apps.accounts.api.viewsets import UserViewSet
from apps.bookclubs.api.viewsets import BookClubViewSet
from apps.books.api.viewsets import BookViewSet
from apps.bookshelfs.api.viewsets import BookShelvesViewSet, BookShelfViewSet, SharedBookShelfViewSet
from apps.bookssimilarities.api.viewsets import BooksSimilaritiesViewSet

router = routers.DefaultRouter()
router.register(r'accounts', UserViewSet, basename='accounts')
router.register(r'bookclubs', BookClubViewSet, basename='bookclubs')
router.register(r'books', BookViewSet, basename='books')
router.register(r'bookshelves', BookShelvesViewSet, basename='bookshelves')
router.register(r'bookshelf', BookShelfViewSet, basename='bookshelf')
router.register(r'shared-bookshelf', SharedBookShelfViewSet, basename='shared_bookshelf')
router.register(r'recommendations', BooksSimilaritiesViewSet, basename='recommendations')

urlpatterns = [path('api/', include(router.urls)),
               path('admin/', admin.site.urls),
               ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
