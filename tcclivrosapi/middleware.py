class SetLastLoginMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        return self.process_response(request=request, response=response)

    def process_response(self, request, response):
        if request.user.is_authenticated:
            # Update last visit time after request finished processing.

            request.user.update_last_login()

        return response
