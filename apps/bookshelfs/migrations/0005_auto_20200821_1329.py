# Generated by Django 3.0.4 on 2020-08-21 13:29

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('bookshelfs', '0004_borrowedbookshelves'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='BorrowedBookShelves',
            new_name='SharedBookShelves',
        ),
    ]
