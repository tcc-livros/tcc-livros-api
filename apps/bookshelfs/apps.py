from django.apps import AppConfig


class BookshelfsConfig(AppConfig):
    name = 'bookshelfs'
