from django.test import TestCase

from apps.accounts.models import User
from apps.books.api.serializers import BookSerializer
from apps.books.models import Book
from apps.bookshelfs.api.serializers import BookShelfSerializer, SharedBookShelfSerializer
from apps.bookshelfs.models import BookShelf, SharedBookShelves


class TestBookshelfSerializer(TestCase):
    def setUp(self) -> None:
        self.book_attrs = dict(ISBN_10='someisbn', ISBN_13="someisbn",
                               title='book title', image_url='image.url.com'
                               )
        self.book = Book.objects.create(**self.book_attrs)
        self.book_serializer = BookSerializer(instance=self.book)

        self.user = User.objects.create_user('user@email.com', 'username', 'pass')

        self.bookshelf_attrs = {'owner': self.user,
                                'book': self.book,
                                'evaluation': None,
                                'available': True,
                                'id': 1
                                }
        self.bookshelf = BookShelf.objects.create(**self.bookshelf_attrs)
        self.bookshelf_attrs['owner'] = self.user.id
        self.bookshelf_attrs['book'] = self.book_serializer.data
        self.bookshelf_serializer = BookShelfSerializer(instance=self.bookshelf)

    def test_contains_expected_fields(self):
        bookshelf_fields = set(self.bookshelf_serializer.data.keys())
        expected_fields = {'owner', 'book', 'evaluation', 'available', 'id'}

        self.assertEqual(bookshelf_fields, expected_fields, 'Fields must be the same')

    def test_fields_content(self):
        self.assertDictEqual(self.bookshelf_serializer.data, self.bookshelf_attrs)


class TestBorrowedBookShelvesSerializer(TestCase):
    def setUp(self) -> None:
        self.book_attrs = dict(ISBN_10='someisbn', ISBN_13="someisbn",
                               title='book title', image_url='image.url.com'
                               )
        self.book = Book.objects.create(**self.book_attrs)
        self.book_serializer = BookSerializer(instance=self.book)

        self.user_1 = User.objects.create_user('user_1@email.com', 'username_1', 'pass')
        self.user_2 = User.objects.create_user('user_2@email.com', 'username_2', 'pass')

        self.bookshelf_attrs = {'owner': self.user_1,
                                'book': self.book,
                                'evaluation': None,
                                'available': True,
                                'id': 1
                                }
        self.bookshelf = BookShelf.objects.create(**self.bookshelf_attrs)
        self.bookshelf_attrs['owner'] = self.user_1.id
        self.bookshelf_attrs['book'] = self.book_serializer.data

        self.borrowed_bookshelf_attrs = {
            "solicitor": self.user_2,
            "bookshelf": self.bookshelf,
        }
        self.borrowed_bookshelf = SharedBookShelves.objects.create(**self.borrowed_bookshelf_attrs)
        self.borrowed_bookshelf_serializer = SharedBookShelfSerializer(instance=self.borrowed_bookshelf)

    def test_contains_expected_fields(self):
        borrowed_bookshelf_fields = set(self.borrowed_bookshelf_serializer.data.keys())
        expected_fields = {'id', 'solicitor', 'bookshelf', 'book', 'owner', 'date_requested', 'date_returned', 'status',
                           'evaluation'}

        self.assertEqual(borrowed_bookshelf_fields, expected_fields, 'Fields must be the same')
