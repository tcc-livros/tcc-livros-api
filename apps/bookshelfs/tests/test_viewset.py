from unittest import mock

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from apps.books.models import Author, Category, Book
from apps.bookshelfs.api.serializers import SharedBookShelfSerializer
from apps.bookshelfs.models import BookShelf, SharedBookShelves

URL_BOOKSHELVES = reverse('bookshelves-list')
URL_BOOKSHELF = reverse('bookshelf-list')
URL_SHARED_BOOKSHELF = reverse('shared_bookshelf-list')


def detail_url(view_name, _id=None, action=None):
    """Return detail of specified url"""
    if action:
        if _id:
            return reverse(f"{view_name}-{action}", args=[_id])
        return reverse(f"{view_name}-{action}")
    return reverse(f'{view_name}-detail', args=[_id])


def create_user_bookshelf(user_email: str, num_of_bookshelves: int) -> None:
    for _id in range(num_of_bookshelves):
        author = Author.objects.create(name=f'author_{_id}')
        category = Category.objects.create(name=f'category_{_id}')

        book = Book.objects.create(

            ISBN_10=f"{_id}" * 10,
            ISBN_13=f"{_id}" * 13,
            title=f'Some book_{_id}',
            image_url=f'imagebook_{_id}.url.com'
        )
        book.authors.add(author)
        book.categories.add(category)
        book.save()
        BookShelf.objects.create(
            owner=get_user_model().objects.get(email=user_email),
            book=book,
        )


def create_shared_bookshelf(**params) -> SharedBookShelves:
    return SharedBookShelves.objects.create(**params)


def create_user(email='user@email.com', password='this pass', name='user name'):
    return get_user_model().objects.create_user(email=email, password=password, name=name)


class TestBookShelvesViewSet(TestCase):

    def setUp(self) -> None:
        self.user = create_user()
        self.client = APIClient()
        self.client.force_authenticate(self.user)
        num_of_bookshelves = 2
        create_user_bookshelf(self.user.email, num_of_bookshelves)

        self.patch_bookshelf = mock.patch('apps.bookshelfs.api.viewsets.BookShelf.create_by_isbn')
        self.mock_bookshelf_create_by_isbn = self.patch_bookshelf.start()

    def tearDown(self) -> None:
        self.patch_bookshelf.stop()

    def test_create_bookshelf_success(self):
        bookshelf = BookShelf.objects.filter(owner=self.user).first()
        self.mock_bookshelf_create_by_isbn.return_value = bookshelf
        bookshelf_data = dict(isbn=bookshelf.book.ISBN_10)
        response = self.client.post(URL_BOOKSHELVES, data=bookshelf_data)
        self.assertEqual(status.HTTP_201_CREATED, response.status_code,
                         'bookshelf must be successfully created')

    def test_create_bookshelf_fails(self):
        self.mock_bookshelf_create_by_isbn.return_value = None
        bookshelf_data = dict(isbn='13 chars isbn')
        response = self.client.post(URL_BOOKSHELVES, data=bookshelf_data)
        message = 'Ops... Algo de errado não está certo! Tente mais tarde'

        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)
        self.assertEqual(message, response.data['message'])
        self.assertIn('message', response.data)

    def test_create_invalid_isbn(self):
        bookshelf_data = dict(isbn='shortisbn')
        response = self.client.post(URL_BOOKSHELVES, data=bookshelf_data)
        message = 'ISBN inválido. ISBN precisa 13 ou 10 dígitos'

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], message)
        self.assertIn('message', response.data)

    def test_get_all_bookshelf_of_owner(self):
        response = self.client.get(URL_BOOKSHELVES)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for bookshef in response.data:
            self.assertFalse(not BookShelf.objects.filter(book__title=bookshef['book']['title']))

    def test_owner_delete_bookshelf(self):
        bookshelf = BookShelf.objects.get(id=1)
        response = self.client.delete(detail_url('bookshelves', bookshelf.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT,
                         'bookshelf must be deleted successfully')

    def test_not_owner_delete_bookshelf(self):
        not_owner_client = APIClient()

        not_owner_user = create_user(email='newuser@email.com', password='this_pass', name='user name')
        not_owner_client.force_authenticate(not_owner_user)

        other_client_response = not_owner_client.get(URL_BOOKSHELVES)
        for bookshelf in other_client_response.data:
            if bookshelf['owner'] != str(not_owner_user.id):
                response = not_owner_client.delete(detail_url('bookshelves', 1))
                self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN,
                                 'only owner must be allowed to delete bookshelf')

    def test_update_evaluation_bookshelf(self):
        bookshelves = self.client.get(URL_BOOKSHELVES).data
        for bookshelf in bookshelves:
            old_evaluation = bookshelf['evaluation']
            new_evaluation = 6
            update_response = self.client.patch(detail_url('bookshelf', bookshelf['id']),
                                                data={"evaluation": new_evaluation})
            self.assertEqual(update_response.status_code, status.HTTP_200_OK,
                             'update evaluation must succeed')
            self.assertNotEqual(old_evaluation, new_evaluation,
                                'old evaluation must be diff than new')

    def test_update_available_bookshelf(self):
        bookshelves = self.client.get(URL_BOOKSHELVES).data
        for bookshelf in bookshelves:
            old_available = bookshelf['available']
            new_available = not bookshelf['available']
            update_response = self.client.patch(detail_url('bookshelves', bookshelf['id']),
                                                data={"available": new_available})
            self.assertEqual(update_response.status_code, status.HTTP_200_OK,
                             'update available must succeed')
            self.assertNotEqual(old_available, new_available,
                                'old available must be diff than new')

    def test_owner_create_same_bookshelf(self):
        bookshelf = BookShelf.objects.filter(owner=self.user).first()
        self.mock_bookshelf_create_by_isbn.return_value = bookshelf
        response = self.client.post(URL_BOOKSHELVES, data={'isbn': bookshelf.book.ISBN_10})

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        bookshelf_id = response.data['bookshelf']['id']
        self.assertEqual(bookshelf.id, bookshelf_id,
                         'bookshelf must me the same')


class TestBookShelfViewSet(TestCase):

    def setUp(self) -> None:
        self.user = create_user()
        self.client = APIClient()
        self.client.force_authenticate(self.user)
        num_of_bookshelves = 2
        create_user_bookshelf(self.user.email, num_of_bookshelves)

        self.patch_bookshelf = mock.patch('apps.bookshelfs.api.viewsets.BookShelf.create_by_isbn')
        self.mock_bookshelf_create_by_isbn = self.patch_bookshelf.start()

    def tearDown(self) -> None:
        self.patch_bookshelf.stop()

    def test_create_bookshelf_success(self):
        create_user_bookshelf(self.user.email, 1)
        book_shelf = BookShelf.objects.get(id=1)
        self.mock_bookshelf_create_by_isbn.return_value = book_shelf
        bookshelf_data = dict(isbn='13 chars isbn')
        response = self.client.post(URL_BOOKSHELF, data=bookshelf_data)
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_create_bookshelf_fails(self):
        self.mock_bookshelf_create_by_isbn.return_value = None
        bookshelf_data = dict(isbn='13 chars isbn')
        response = self.client.post(URL_BOOKSHELF, data=bookshelf_data)
        message = 'Ops... Algo de errado não está certo! Tente mais tarde'
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)
        self.assertEqual(message, response.data['message'])
        self.assertIn('message', response.data)

    def test_create_invalid_isbn(self):
        bookshelf_data = dict(isbn='shortisbn')
        response = self.client.post(URL_BOOKSHELF, data=bookshelf_data)
        message = 'ISBN inválido. ISBN precisa 13 ou 10 dígitos'

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], message)
        self.assertIn('message', response.data)

    def test_get_all_bookshelf_of_owner(self):
        response = self.client.get(URL_BOOKSHELF)
        user = get_user_model().objects.get(email=self.user.email)
        if response.status_code == status.HTTP_200_OK:
            for bookshelf in response.data:
                self.assertEqual(bookshelf['owner'], user.id,
                                 'must only get the owners bookshelves')
        else:
            self.assertEqual(response.status_code, status.HTTP_200_OK,
                             'status code must be 200')

    def test_owner_delete_bookshelf(self):
        response = self.client.get(URL_BOOKSHELF)
        for bookshelf in response.data:
            delete_response = self.client.delete(detail_url('bookshelf', bookshelf["id"]))
            self.assertEqual(delete_response.status_code, status.HTTP_204_NO_CONTENT,
                             'bookshelf must be deleted successfully')

    def test_not_owner_delete_bookshelf(self):
        new_user = create_user(email='newuser@email.com', password='this_pass', name='user name')

        new_client = APIClient()
        new_client.force_authenticate(new_user)

        other_client_response = new_client.get(URL_BOOKSHELF)
        for bookshelf in other_client_response.data:
            if bookshelf['owner'] != new_user.id:
                delete_response = new_client.delete(detail_url('bookshelf', bookshelf["id"]))
                self.assertEqual(delete_response.status_code,
                                 status.HTTP_403_FORBIDDEN)

    def test_update_evaluation_bookshelf(self):
        book_shelves = BookShelf.objects.filter(owner=self.user)
        for bookshelf in book_shelves:
            old_evaluation = bookshelf.evaluation
            new_evaluation = 6
            update_response = self.client.patch(detail_url('bookshelf', bookshelf.id),
                                                data={"evaluation": new_evaluation})
            self.assertEqual(update_response.status_code, status.HTTP_200_OK, 'update evaluation must succeed')
            self.assertNotEqual(old_evaluation, new_evaluation, 'old evaluation must be diff than new')

    def test_update_available_bookshelf(self):
        bookshelves = BookShelf.objects.filter(owner=self.user)
        for bookshelf in bookshelves:
            old_available = bookshelf.available
            new_available = not bookshelf.available
            update_response = self.client.patch(detail_url('bookshelf', bookshelf.id),
                                                data={"available": new_available})
            self.assertEqual(update_response.status_code, status.HTTP_200_OK,
                             'update available must succeed')
            self.assertNotEqual(old_available, new_available,
                                'old available must be diff than new')

    def test_owner_create_same_bookshelf(self):
        create_user_bookshelf(self.user.email, 1)
        bookshelf = BookShelf.objects.filter(owner=self.user).first()
        self.mock_bookshelf_create_by_isbn.return_value = bookshelf
        isbn = bookshelf.book.ISBN_10
        bookshelf_data = dict(isbn=isbn)
        response = self.client.post(URL_BOOKSHELF, data=bookshelf_data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(status.HTTP_201_CREATED, response.status_code, 'user must not add same book to shelf')
        bookshelf_id = response.data['bookshelf']['id']
        self.assertEqual(bookshelf.id, bookshelf_id, 'bookshelf must me the same')


class TestSharedBookShelfViewSet(TestCase):

    def setUp(self) -> None:
        self.user = create_user()
        self.client = APIClient()
        self.client.force_authenticate(self.user)
        num_of_bookshelves = 2
        create_user_bookshelf(self.user.email, num_of_bookshelves)

        self.solicitor_user = create_user('solicitor@email.com', 'solicitorpass', 'solicitor name')
        self.solicitor_client = APIClient()
        self.solicitor_client.force_authenticate(self.solicitor_user)

        self.status_shared_bookshelf = {'requested': 0,
                                        'borrowed': 1,
                                        'returned': 2,
                                        'denied': 3,
                                        }

    def test_create_shared_bookshelf_success(self):
        bookshelf = BookShelf.objects.get(id=1)
        bookshelf_data = {'bookshelf': bookshelf.id}
        response = self.client.post(URL_SHARED_BOOKSHELF, data=bookshelf_data)
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_ask_again_denied_shared_bookshelf_success(self):
        bookshelf = BookShelf.objects.get(id=1)
        bookshelf_data = {'bookshelf': bookshelf.id}
        shared_bookshelf = SharedBookShelves.objects.create(solicitor=self.user,
                                                            bookshelf=bookshelf,
                                                            status=self.status_shared_bookshelf['denied'])
        response = self.client.post(URL_SHARED_BOOKSHELF, data=bookshelf_data)
        self.assertEqual(status.HTTP_201_CREATED, response.status_code, 'new solicitation must be created')
        self.assertNotEqual(shared_bookshelf.id, response.data['id'], 'ids of borrowed_bookshelf must be different')

    def test_ask_again_returned_shared_bookshelf_success(self):
        bookshelf = BookShelf.objects.get(id=1)
        bookshelf_data = {'bookshelf': bookshelf.id}
        shared_bookshelf = SharedBookShelves.objects.create(solicitor=self.user,
                                                            bookshelf=bookshelf,
                                                            status=self.status_shared_bookshelf['returned'])
        response = self.client.post(URL_SHARED_BOOKSHELF, data=bookshelf_data)
        self.assertEqual(status.HTTP_201_CREATED, response.status_code, 'new solicitation must be created')
        self.assertNotEqual(shared_bookshelf.id, response.data['id'], 'ids of borrowed_bookshelf must be different')

    def test_shared_bookshelf_fail(self):
        bookshelf = BookShelf.objects.get(id=1)
        bookshelf_data = {'bookshelf': bookshelf.id}
        SharedBookShelves.objects.create(solicitor=self.user,
                                         bookshelf=bookshelf)
        response = self.client.post(URL_SHARED_BOOKSHELF, data=bookshelf_data)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code, 'solicitation cant be created')
        self.assertEqual(response.data['message'], "Solicitação de livro em aberto")

    def test_shared_bookshelf_action_borrow(self):
        bookshelf = BookShelf.objects.filter(owner=self.user).first()
        shared_bookshelf = create_shared_bookshelf(solicitor=self.solicitor_user, bookshelf=bookshelf)

        response = self.solicitor_client.patch(detail_url('shared_bookshelf', shared_bookshelf.id, action='borrow'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bookshelf = BookShelf.objects.filter(owner=self.user).first()
        self.assertFalse(bookshelf.available, 'Bookshelf must be unavailable')

        response = self.client.patch(detail_url('shared_bookshelf', shared_bookshelf.id, action='borrow'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_shared_bookshelf_action_return(self):
        bookshelf = BookShelf.objects.filter(owner=self.user).first()
        shared_bookshelf = create_shared_bookshelf(solicitor=self.solicitor_user, bookshelf=bookshelf)

        response = self.client.patch(detail_url('shared_bookshelf', shared_bookshelf.id, action='return'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bookshelf = BookShelf.objects.filter(owner=self.user).first()
        self.assertTrue(bookshelf.available, 'Bookshelf must be unavailable')

        response = self.solicitor_client.patch(detail_url('shared_bookshelf', shared_bookshelf.id, action='return'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_shared_bookshelf_action_deny(self):
        bookshelf = BookShelf.objects.filter(owner=self.user).first()
        shared_bookshelf = create_shared_bookshelf(solicitor=self.solicitor_user, bookshelf=bookshelf)

        response = self.client.patch(detail_url('shared_bookshelf', shared_bookshelf.id, action='deny'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.solicitor_client.patch(detail_url('shared_bookshelf', shared_bookshelf.id, action='deny'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_shared_bookshelf_endpoint_methods_allowed(self):
        res_put = self.client.put(URL_SHARED_BOOKSHELF)
        res_patch = self.client.patch(URL_SHARED_BOOKSHELF)
        res_delete = self.client.delete(URL_SHARED_BOOKSHELF)

        self.assertEqual(res_put.status_code, status.HTTP_405_METHOD_NOT_ALLOWED,
                         'status code must be 405 METHOD NOT ALLOWED')
        self.assertEqual(res_patch.status_code, status.HTTP_405_METHOD_NOT_ALLOWED,
                         'status code must be 405 METHOD NOT ALLOWED')
        self.assertEqual(res_delete.status_code, status.HTTP_405_METHOD_NOT_ALLOWED,
                         'status code must be 405 METHOD NOT ALLOWED')

        res_put = self.solicitor_client.put(URL_SHARED_BOOKSHELF)
        res_patch = self.solicitor_client.patch(URL_SHARED_BOOKSHELF)
        res_delete = self.solicitor_client.delete(URL_SHARED_BOOKSHELF)

        self.assertEqual(res_put.status_code, status.HTTP_405_METHOD_NOT_ALLOWED,
                         'status code must be 405 METHOD NOT ALLOWED')
        self.assertEqual(res_patch.status_code, status.HTTP_405_METHOD_NOT_ALLOWED,
                         'status code must be 405 METHOD NOT ALLOWED')
        self.assertEqual(res_delete.status_code, status.HTTP_405_METHOD_NOT_ALLOWED,
                         'status code must be 405 METHOD NOT ALLOWED')

    def test_retrieve_shared_bookshelves_action_borrowed(self):
        for bookshelf in BookShelf.objects.all():
            create_shared_bookshelf(solicitor=self.solicitor_user, bookshelf=bookshelf)

        serializer = SharedBookShelfSerializer(SharedBookShelves.objects.all(), many=True)
        response = self.client.get(detail_url('shared_bookshelf', action='borrowed'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_retrieve_shared_bookshelves_action_solicited(self):
        for bookshelf in BookShelf.objects.all():
            create_shared_bookshelf(solicitor=self.solicitor_user, bookshelf=bookshelf)

        serializer = SharedBookShelfSerializer(SharedBookShelves.objects.all(), many=True)
        response = self.solicitor_client.get(detail_url('shared_bookshelf', action='solicited'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)
