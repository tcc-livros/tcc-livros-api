from unittest import mock

from django.test import TestCase
from rest_framework import status

from apps.bookshelfs.search_module import google_api as g_api

BOOK_DATA_CONTENT = {
    "kind": "books#volumes",
    "totalItems": 1,
    "items": [
        {
            "kind": "books#volume",
            "id": "_qCbuAEACAAJ",
            "etag": "10SZF05A8E4",
            "selfLink": "https://www.googleapis.com/books/v1/volumes/_qCbuAEACAAJ",
            "volumeInfo": {
                "title": "Comentário Ao Sobre A Interpretação De Aristóteles",
                "authors": [
                    "Sto Tomás De Aquino",
                    "Bernardo Veiga",
                    "Paulo Faintanin"
                ],
                "description": "Com esta edição do comentário de Tomás ao Sobre a interpretação de Aristóteles, a VIDE Editorial dá continuidade ao projeto Tomás, comentador, que tem a intenção de publicar uma série de textos inéditos, com breve apresentação e notas à tradução, cujo intuito é de pouco ou nada interferir na obra, deixando o leitor com o mínimo necessário para ir ele mesmo ao texto do Aquinate.",
                "industryIdentifiers": [
                    {
                        "type": "ISBN_10",
                        "identifier": "8595070407"
                    },
                    {
                        "type": "ISBN_13",
                        "identifier": "9788595070400"
                    }
                ],
                "readingModes": {
                    "text": False,
                    "image": False
                },
                "pageCount": 196,
                "printType": "BOOK",
                "maturityRating": "NOT_MATURE",
                "allowAnonLogging": False,
                "contentVersion": "preview-1.0.0",
                "panelizationSummary": {
                    "containsEpubBubbles": False,
                    "containsImageBubbles": False
                },
                "language": "pt",
                "previewLink": "http://books.google.com.br/books?id=_qCbuAEACAAJ&dq=isbn:9788595070400&hl=&cd=1&source=gbs_api",
                "infoLink": "http://books.google.com.br/books?id=_qCbuAEACAAJ&dq=isbn:9788595070400&hl=&source=gbs_api",
                "canonicalVolumeLink": "https://books.google.com/books/about/Coment%C3%A1rio_Ao_Sobre_A_Interpreta%C3%A7%C3%A3o_D.html?hl=&id=_qCbuAEACAAJ"
            },
            "saleInfo": {
                "country": "BR",
                "saleability": "NOT_FOR_SALE",
                "isEbook": False
            },
            "accessInfo": {
                "country": "BR",
                "viewability": "NO_PAGES",
                "embeddable": False,
                "publicDomain": False,
                "textToSpeechPermission": "ALLOWED",
                "epub": {
                    "isAvailable": False
                },
                "pdf": {
                    "isAvailable": False
                },
                "webReaderLink": "http://play.google.com/books/reader?id=_qCbuAEACAAJ&hl=&printsec=frontcover&source=gbs_api",
                "accessViewStatus": "NONE",
                "quoteSharingAllowed": False
            },
            "searchInfo": {
                "textSnippet": "Com esta edição do comentário de Tomás ao Sobre a interpretação de Aristóteles, a VIDE Editorial dá continuidade ao projeto Tomás, comentador, que tem a intenção de publicar uma série de textos inéditos, com breve ..."
            }
        }
    ]
}

BOOK_DATA_CONTENT_NO_CATEGORIES = {
    "kind": "books#volumes",
    "totalItems": 1,
    "items": [{
        "volumeInfo": {
            "title": "Comentário Ao Sobre A Interpretação De Aristóteles",
            "authors": [
                "Sto Tomás De Aquino",
                "Bernardo Veiga",
                "Paulo Faintanin"
            ],
            "industryIdentifiers": [
                {
                    "type": "ISBN_10",
                    "identifier": "8595070407"
                },
                {
                    "type": "ISBN_13",
                    "identifier": "9788595070400"
                }
            ],
            "imageLinks": {
                "smallThumbnail": "http://books.google.com/books/content?id=raW-65SeIw0C&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api",
                "thumbnail": "http://books.google.com/books/content?id=raW-65SeIw0C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api"
            },
        },
    }
    ]
}


class TestSearchBookGoogleApi(TestCase):
    def setUp(self) -> None:
        self.urllib_patch = mock.patch('apps.bookshelfs.search_module.google_api.urllib.request.urlopen')
        self.urlopen_mock = self.urllib_patch.start()
        self.json_patch = mock.patch('apps.bookshelfs.search_module.google_api.json.loads')
        self.json_loads_mock = self.json_patch.start()

    def tearDown(self) -> None:
        self.urllib_patch.stop()
        self.json_patch.stop()

    def test_isbn_13_ok(self):
        self.json_loads_mock.return_value = {"totalItems": 1}
        type(self.urlopen_mock.return_value).code = status.HTTP_200_OK

        ISBN_13_OK = 'valid isnb 13'
        response, status_response = g_api.search_book_google_api(ISBN_13_OK)

        self.assertEqual(status_response, status.HTTP_200_OK)
        self.assertNotEqual(response['totalItems'], 0, 'valid isbn_13 must have more than 0 items')

    def test_isbn_10_ok(self):
        self.json_loads_mock.return_value = {"totalItems": 1}
        type(self.urlopen_mock.return_value).code = status.HTTP_200_OK

        ISBN_10_OK = 'valid isbn 10'
        response, status_response, = g_api.search_book_google_api(ISBN_10_OK)
        self.assertEqual(status_response, status.HTTP_200_OK)
        self.assertNotEqual(response['totalItems'], 0, 'valid isbn_10 must have more than 0 items')

    def test_isbn_13_bad(self):
        self.json_loads_mock.return_value = {"totalItems": 0}
        type(self.urlopen_mock.return_value).code = status.HTTP_200_OK

        ISBN_13_BAD = 'invalid isnb 13'

        response, status_response = g_api.search_book_google_api(ISBN_13_BAD)
        self.assertEqual(status_response, status.HTTP_200_OK)
        self.assertEqual(response['totalItems'], 0, 'invalid isbn_13 must have 0 items')

    def test_isbn_10_bad(self):
        self.json_loads_mock.return_value = {"totalItems": 0}
        type(self.urlopen_mock.return_value).code = status.HTTP_200_OK

        ISBN_10_BAD = 'invalid isnb 10'

        response, status_response = g_api.search_book_google_api(ISBN_10_BAD)
        self.assertEqual(status_response, status.HTTP_200_OK)
        self.assertEqual(response['totalItems'], 0, 'invalid isbn_10 must have 0 items')


class TestGetSmallThumbnail(TestCase):
    def test_with_image(self):
        small_thumbnail_url = 'url.image.com'
        volume_info = dict(imageLinks=dict(smallThumbnail=small_thumbnail_url))

        image_url = g_api.get_small_thumbnail(volume_info)
        self.assertEqual(image_url, small_thumbnail_url,
                         'get() must have image url')

    def test_with_no_image(self):
        volume_info = {'': ''}
        image_url = g_api.get_small_thumbnail(volume_info)
        self.assertEqual(image_url, '',
                         'get() must have no image url')


class TestGetISBN(TestCase):
    def test_get_isbns_of_volume_info(self):
        volume_info = dict(industryIdentifiers=[
            dict(
                type='ISBN_13',
                identifier='ISBN_13_OK'
            ),
            dict(
                type='ISBN_10',
                identifier='ISBN_10_OK'
            )
        ])
        isbns = g_api.get_isbns(volume_info)
        self.assertEqual(isbns, ('ISBN_13_OK', 'ISBN_10_OK'),
                         'isbns must be equals')


class TestGetBookData(TestCase):
    def setUp(self) -> None:
        self.patch_get_isbns = mock.patch('apps.bookshelfs.search_module.google_api.get_isbns')
        self.mock_get_isbns = self.patch_get_isbns.start()

        self.patch_get_small_thumbnail = mock.patch('apps.bookshelfs.search_module.google_api.get_small_thumbnail')
        self.mock_get_small_thumbnail = self.patch_get_small_thumbnail.start()

        self.patch_search_book_google_api = mock.patch(
            'apps.bookshelfs.search_module.google_api.search_book_google_api')
        self.mock_search_book_google_api = self.patch_search_book_google_api.start()

    def tearDown(self) -> None:
        self.patch_get_isbns.stop()
        self.patch_get_small_thumbnail.stop()
        self.patch_search_book_google_api.stop()

    def test_get_book_complete_with_http_200_with_categories(self):
        self.mock_get_isbns.return_value = ('ISBN_13_OK', 'ISBN_10_OK')
        self.mock_get_small_thumbnail.return_value = 'image.url.com'
        self.mock_search_book_google_api.return_value = (BOOK_DATA_CONTENT, status.HTTP_200_OK)

        book = g_api.get_book_data('ISBN_10_OK')
        book_fields = set(book.keys())
        expected_fields_set = {'title', 'authors', 'ISBN_13', 'ISBN_10', 'categories', 'image_url'}
        self.assertEqual(book_fields, expected_fields_set,
                         'fields received must be equal')
        self.assertEqual(book['ISBN_13'], 'ISBN_13_OK')
        self.assertEqual(book['ISBN_10'], 'ISBN_10_OK')

    def test_get_book_complete_with_http_200_with_no_categories(self):
        self.mock_get_isbns.return_value = ('ISBN_13_OK', 'ISBN_10_OK')
        self.mock_get_small_thumbnail.return_value = 'image.url.com'
        self.mock_search_book_google_api.return_value = (BOOK_DATA_CONTENT_NO_CATEGORIES, status.HTTP_200_OK)

        book = g_api.get_book_data('ISBN_10_OK')
        book_fields = set(book.keys())
        expected_fields_set = {'title', 'authors', 'ISBN_13', 'ISBN_10', 'categories', 'image_url'}
        self.assertEqual(book_fields, expected_fields_set,
                         'fields received must be equal')
        self.assertEqual(book['categories'], '', 'categories must be "" ')
        self.assertEqual(book['ISBN_13'], 'ISBN_13_OK')
        self.assertEqual(book['ISBN_10'], 'ISBN_10_OK')

    def test_get_book_complete_with_http_200_with_no_book(self):
        self.mock_search_book_google_api.return_value = ({"kind": "books#volumes", "totalItems": 0},
                                                         status.HTTP_200_OK)

        book = g_api.get_book_data('ISBN_10_OK')
        self.assertEqual(book, {}, 'totalItems==0 must return empty dict')

    def test_get_book_complete_with_http_not_200(self):
        self.mock_search_book_google_api.return_value = ({}, False)

        book = g_api.get_book_data('ISBN_10_OK')
        self.assertEqual(book, {}, 'book must be {}')
