import json
import urllib.request


def search_book_google_api(ISBN):
    search_url = f"https://www.googleapis.com/books/v1/volumes?q=isbn:{ISBN}"
    response = urllib.request.urlopen(search_url)
    data = json.loads(response.read().decode())
    return data, response.code


def get_small_thumbnail(volume_info):
    image_links = volume_info.get('imageLinks', None)
    if image_links is None:
        return ""
    return image_links['smallThumbnail']


def get_isbns(volume_info):
    isbns = {}
    for isbn in volume_info['industryIdentifiers']:
        isbns[isbn['type']] = isbn['identifier']
    return isbns['ISBN_13'], isbns['ISBN_10']


def get_book_data(ISBN):
    book = {}
    data_content, status = search_book_google_api(ISBN)
    http_ok = 200

    if status == http_ok and data_content["totalItems"] != 0:
        volume_info = data_content['items'][0]['volumeInfo']
        book['title'] = volume_info['title']
        book['authors'] = volume_info['authors']

        book['ISBN_13'], book['ISBN_10'] = get_isbns(volume_info)

        book['categories'] = volume_info.get('categories', None)
        if book['categories'] is None:
            book['categories'] = ""
        book['image_url'] = get_small_thumbnail(volume_info)

    return book
