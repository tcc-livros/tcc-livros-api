from django.db import models
from django.db.models import Q
from django.utils.datetime_safe import date

from apps.books.models import Book
from tcclivrosapi import settings


class BookShelf(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    evaluation = models.IntegerField(null=True, blank=True)
    available = models.BooleanField(default=True)

    def __str__(self):
        return f"BookShelf of {self.owner}"

    @classmethod
    def create_by_isbn(cls, isbn, user):

        book = Book.objects.filter(Q(ISBN_10=isbn) | Q(ISBN_13=isbn)).first()
        if book is not None:
            existing_bookshelf = cls.objects.filter(book=book, owner=user).first()
            if existing_bookshelf:
                return existing_bookshelf
            bookshelf = cls(owner=user, book=book)
            bookshelf.save()
            return bookshelf

        book = Book.create_by_isbn(isbn)
        if book is not None:
            bookshelf = cls(owner=user, book=book)
            bookshelf.save()
            return bookshelf
        return None


class SharedBookShelves(models.Model):
    solicitor = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    bookshelf = models.ForeignKey(BookShelf, on_delete=models.CASCADE)
    date_requested = models.DateField(default=date.today)
    date_returned = models.DateField(null=True, blank=True)
    STATUS = [
        (0, 'requested'),
        (1, 'borrowed'),
        (2, 'returned'),
        (3, 'denied')
    ]
    status = models.SmallIntegerField(choices=STATUS, default=0)
    evaluation = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return f"'{self.bookshelf.owner}' borrow '{self.bookshelf.book.title}' to '{self.solicitor}' "
