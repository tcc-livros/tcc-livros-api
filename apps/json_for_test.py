BOOKS_EVALUATIONS = {'A Vida Intelectual': {'Ana': 25,
                                            'Marcos:': 30,
                                            'Pedro': 25,
                                            'Adriano': 30,
                                            'Janaina': 30},

                     'O Trabalho Intelectual': {'Ana': 35,
                                                'Marcos': 35,
                                                'Pedro': 30,
                                                'Claudia': 35,
                                                'Adriano': 40,
                                                'Janaina': 40,
                                                'Leonardo': 45},

                     'Gramática Metódica da Língua Portuguesa': {'Ana': 30,
                                                                 'Marcos:': 15,
                                                                 'Claudia': 30,
                                                                 'Adriano': 20},

                     'Imitação de Cristo': {'Ana': 35,
                                            'Marcos:': 50,
                                            'Pedro': 35,
                                            'Claudia': 40,
                                            'Adriano': 30,
                                            'Janaina': 50,
                                            'Leonardo': 40},

                     'As idéias têm Consequências': {'Ana': 25,
                                                     'Marcos:': 30,
                                                     'Claudia': 25,
                                                     'Adriano': 20,
                                                     'Janaina': 35,
                                                     'Leonardo': 10},

                     'A Educação da Vontade': {'Ana': 30,
                                               'Marcos:': 35,
                                               'Pedro': 40,
                                               'Claudia': 45,
                                               'Adriano': 30,
                                               'Janaina': 30},

                     'El Criterio': {'Marcos': 35,
                                     'Claudia': 35,
                                     'Adriano': 40,
                                     'Leonardo': 45},

                     'Caminho de perfeição': {'Ana': 25,
                                              'Pedro': 25,
                                              'Janaina': 30}
                     }

EUCLIDEANS = [1.0, 0.09090909090909091, 0.08209951522176571, 0.0568727151524708, 0.05255480363370354,
              0.050740077174411764, 0.030653430031715508, 0.0625, 0.046263038037944106, 0.045018270457898496,
              0.022721268975236956, 0.16666666666666666, 0.03580914922054215, 0.025589279178274353, 0.07548623297930128,
              0.03922353493794955, 0.03641990430707569, 0.022144076106536292, 0.04003337563861254, 0.023511090089669593,
              0.06604088253131131, 0.05948348715197548, 0]

A_VIDA_INTELECTUAL_SIMILARITIES = [(0.09090909090909091, 'Milenium'),
                                   (0.08209951522176571, 'Norbit'), (0.0568727151524708, 'Star Wars'),
                                   (0.05255480363370354, 'O Ultimato Bourne'), (0.050740077174411764, 'Star Trek'),
                                   (0.030653430031715508, 'Exterminador do Futuro')]
