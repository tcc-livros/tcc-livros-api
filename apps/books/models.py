from django.db import models

from apps.bookshelfs.search_module import google_api


class Author(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Book(models.Model):
    ISBN_10 = models.CharField(max_length=20, null=True, blank=True)
    ISBN_13 = models.CharField(max_length=20, null=True, blank=True)
    title = models.CharField(max_length=100)
    authors = models.ManyToManyField(Author)
    categories = models.ManyToManyField(Category)
    image_url = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.title = self.title.lower()
        return super(Book, self).save(*args, **kwargs)

    def euclidean_other_book(self, other):
        from math import sqrt
        from apps.bookshelfs.models import BookShelf, SharedBookShelves
        from django.db.models import F

        if self.pk == other.pk:
            return 1

        bookshelves_and_shared_bookshelves = list(BookShelf.objects.filter(book=self.pk).values("owner", "evaluation"))
        bookshelves_and_shared_bookshelves += SharedBookShelves.objects.filter(bookshelf__id=self.pk).annotate(
            owner=F("solicitor")).values("owner", "evaluation")

        other_bookshelves_and_shared_bookshelves = list(
            BookShelf.objects.filter(book=other.pk).values("owner", "evaluation"))
        other_bookshelves_and_shared_bookshelves += SharedBookShelves.objects.filter(bookshelf__id=other.pk).annotate(
            owner=F("solicitor")).values("owner", "evaluation")

        _sum = 0
        for bookshelf in bookshelves_and_shared_bookshelves:
            other_bookshelf = self._get_other_bookshelf_same_owner(bookshelf, other_bookshelves_and_shared_bookshelves)
            if other_bookshelf:
                _sum += pow(bookshelf['evaluation'] - other_bookshelf['evaluation'], 2)

        return 1 / (1 + sqrt(_sum)) if _sum != 0 else _sum

    def _get_other_bookshelf_same_owner(self, bookshelf, others_bookshelves):
        return next(iter(other_bookshelf for other_bookshelf in others_bookshelves if
                         other_bookshelf['owner'] == bookshelf['owner'] or []), None)

    def get_similarities(self, num=False):
        similarities = []
        for other_book in Book.objects.exclude(pk=self.pk):
            euclidean = self.euclidean_other_book(other_book)
            if euclidean != 0:
                similarities.append((euclidean, other_book))
        similarities.sort(key=lambda simi: simi[0], reverse=True)

        if num:
            return similarities[:num]

        return similarities

    @classmethod
    def create_by_isbn(cls, isbn):
        book_dict = google_api.get_book_data(isbn)
        if not book_dict:
            return None
        else:
            book = cls(
                ISBN_10=book_dict['ISBN_10'],
                ISBN_13=book_dict['ISBN_13'],
                title=book_dict['title'],
                image_url=book_dict['image_url'],
            )
            book.save()
            for author in book_dict['authors']:
                author, created = Author.objects.get_or_create(name=author)
                author.save()
                book.authors.add(author)
            for category in book_dict['categories']:
                category, created = Category.objects.get_or_create(name=category)
                category.save()
                book.categories.add(category)
                book.save()
            return book
