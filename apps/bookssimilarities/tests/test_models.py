import random

from django.test import TestCase

from apps.accounts.models import User
from apps.books.models import Book
from apps.bookshelfs.models import BookShelf, SharedBookShelves
from apps.bookssimilarities.models import BooksSimilarities
from apps.json_for_test import BOOKS_EVALUATIONS, EUCLIDEANS


class TestSimilarities(TestCase):

    def create_user(self, name: str) -> User:
        return User.objects.create_user(name + "@email.com", name, "this pass")

    def setUp(self) -> None:
        SharedBookShelves.objects.all().delete()
        BookShelf.objects.all().delete()
        Book.objects.all().delete()

        users = []
        for book in BOOKS_EVALUATIONS:
            _book = Book.objects.create(title=book)

            for user_name in BOOKS_EVALUATIONS[book]:
                if user_name not in users:
                    users.append(user_name)
                    user = self.create_user(user_name)
                    evaluation = BOOKS_EVALUATIONS[book][user_name]
                    BookShelf.objects.create(owner=user, book=_book, evaluation=evaluation)

                else:
                    if BookShelf.objects.filter(book=_book).exists():
                        user = User.objects.get(email=user_name + "@email.com")
                        bookshelf = random.choice(BookShelf.objects.filter(book=_book))
                        evaluation = BOOKS_EVALUATIONS[book][user_name]
                        SharedBookShelves.objects.create(solicitor=user, bookshelf=bookshelf, evaluation=evaluation)
                    else:
                        user = User.objects.get(email=user_name + "@email.com")
                        evaluation = BOOKS_EVALUATIONS[book][user_name]
                        BookShelf.objects.create(owner=user, book=_book, evaluation=evaluation)

    def test_similarities(self):
        BooksSimilarities.update_all_similarities()
        books_similarities = BooksSimilarities.objects.all()
        self.assertNotEqual(len(books_similarities), 0)
        for books_similarities in BooksSimilarities.objects.all():
            self.assertTrue(books_similarities.similarity in EUCLIDEANS)
