from django.db import models

from apps.books.models import Book


class BooksSimilarities(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE, related_name="book_similar")
    similar = models.ForeignKey(Book, on_delete=models.CASCADE, related_name="similar_book")
    similarity = models.FloatField(null=True, blank=True)

    def __str__(self):
        return f"{self.book.title} is {self.similarity} to {self.similar.title}"

    def update_similarity(self, similarity):
        self.similarity = similarity
        self.save(update_fields=['similarity'])

    @classmethod
    def update_all_similarities(cls):
        BooksSimilarities.objects.all()
        for book in Book.objects.all():
            for similarity, similar_book in book.get_similarities():
                books_similarities, is_created = BooksSimilarities.objects.get_or_create(book_id=book.id,
                                                                                         similar_id=similar_book.id)
                books_similarities.update_similarity(similarity)
