# Generated by Django 3.0.4 on 2020-04-02 16:49

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ('books', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BooksSimilarities',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('similarity', models.FloatField()),
                ('book', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='book_similar',
                                           to='books.Book')),
                ('similar', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='similar_book',
                                              to='books.Book')),
            ],
        ),
    ]
