from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from apps.bookshelfs.models import BookShelf
from apps.bookssimilarities.models import BooksSimilarities


class BooksSimilaritiesViewSet(ModelViewSet):
    authentication_classes = (TokenAuthentication,)
    queryset = BooksSimilarities.objects.all()
    permission_classes = (IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        book_shelves = BookShelf.objects.filter(owner=self.request.user.pk)

        recommendations = []
        for book_shelf in book_shelves:
            recommendations += book_shelf.book.get_similarities()
        recommendations = set(recommendations)
        recommendations = sorted(recommendations, reverse=True, key=lambda el: el[0])

        max_recommendations = 5
        recommendations = recommendations[:max_recommendations]

        recommendations = [
            dict(title=book.title,
                 authors=[author.name for author in book.authors.all()],
                 image_url=book.image_url,
                 similarity=similarity
                 ) for similarity, book in recommendations
        ]

        return Response({"recomendations": recommendations})
