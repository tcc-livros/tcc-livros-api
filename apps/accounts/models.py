from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models, transaction
from django.utils.timezone import now


class UserManager(BaseUserManager):
    """"Manager for MyUser"""

    def create_user(self, email, name, password=None, **kwargs):
        """Create a new user profile"""
        if not email:
            raise ValueError("User must have a email address")
        if not name:
            raise ValueError("User must have a name")
        email = self.normalize_email(email)
        user = self.model(email=email, name=name, **kwargs)

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, name, password):
        """Create and save a new superuser with given details"""
        user = self.create_user(email=email, name=name, password=password)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser, PermissionsMixin):
    """Database model for users in the system"""

    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    telegram = models.CharField(max_length=32, null=True, blank=True, unique=True)
    # TODO configurar path onde as fotos são salvas
    image = models.ImageField(upload_to="myusers", null=True, blank=True)
    bio = models.CharField(max_length=100, null=True, blank=True)
    evaluation = models.FloatField(default=0)
    standard = models.FloatField(default=0)
    num_of_evaluations = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=now)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    def get_full_name(self):
        """get the full name of the user"""
        return self.name

    def get_short_name(self):
        """get only the first name of the user"""
        return self.name.split(' ')[0]

    def __str__(self):
        return self.email

    @transaction.atomic
    def update_evaluation(self, value):
        prev_evaluation = self.evaluation
        self.num_of_evaluations += 1
        self.evaluation = self.evaluation + ((value - self.evaluation) / self.num_of_evaluations)
        self.standard = self.standard + (value - self.evaluation) * (value - prev_evaluation)

        self.save()

    def update_last_login(self):
        self.last_login = now()
        self.save(update_fields=['last_login'])
