from rest_framework import serializers
from rest_framework.authtoken.models import Token

from ..models import User


class UserSerializer(serializers.ModelSerializer):
    """Serializers a my_user objetc"""
    num_of_books = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ["name", 'num_of_books', 'telegram', "email", 'evaluation', 'bio', 'image', 'last_login', 'created_at']

    def get_num_of_books(self, obj):
        return obj.bookshelf_set.count()

    def update(self, instance, validated_data):
        evaluation = validated_data.get('evaluation', None)
        if evaluation:
            instance.update_evaluation(evaluation)
        instance.save()

        return instance


class UserDetailSerializer(serializers.ModelSerializer):
    """Detail serializer of my_user object"""
    num_of_books = serializers.SerializerMethodField()
    token = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ["name", 'num_of_books', 'telegram', "email", 'evaluation', 'bio', 'image', 'last_login',
                  'created_at', 'password', 'token']
        extra_kwargs = {
            'password': {
                'write_only': True,
            },
            'last_login': {
                'read_only': True,
            },
            'created_at': {
                'read_only': True,
            },
            'token': {
                'read_only': True
            }
        }

    def get_token(self, obj):
        token, _ = Token.objects.get_or_create(user=obj)
        return token.key

    def get_num_of_books(self, obj):
        return obj.bookshelf_set.count()

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()

        return instance

    def create(self, validated_data):
        """Create and return a new user"""
        user = User.objects.create_user(**validated_data)
        Token.objects.get_or_create(user=user)
        return user
