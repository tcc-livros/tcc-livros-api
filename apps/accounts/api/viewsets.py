from django.contrib.auth import authenticate
from rest_framework import filters, mixins
from rest_framework import status
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from . import permissions
from .serializers import UserSerializer, UserDetailSerializer
from ..models import User


class UserViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin, mixins.RetrieveModelMixin, mixins.UpdateModelMixin,
                  mixins.ListModelMixin):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, permissions.UpdateOwnUser)
    filter_backends = (filters.SearchFilter,)
    search_fields = ("name", 'email',)

    def get_serializer_class(self):
        """Return appropriate serializer class"""
        if self.action in ['register', 'login', 'partial_update']:
            return UserDetailSerializer
        elif self.action == 'retrieve' and str(self.request.user.id) == self.kwargs['pk']:
            return UserDetailSerializer

        return self.serializer_class

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset().exclude(id=self.request.user.id))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=["post", ], detail=True, permission_classes=[IsAuthenticated])
    def evaluate(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

    @action(methods=["post", ], detail=False, permission_classes=[], authentication_classes=[])
    def login(self, request, *args, **kwargs):
        is_missing, field = missing_fields(request.data, 'password', 'email')
        if is_missing:
            return Response({"message": f"campo '{field}' é obrigatório."}, status=status.HTTP_400_BAD_REQUEST)

        user = authenticate(email=request.data['email'],
                            password=request.data['password'])
        if user:
            serializer = self.get_serializer(user)
            return Response({"message": "Login efetuado com sucesso", 'user': {**serializer.data}})

        return Response({"message": "Senha ou email inválido"},
                        status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['post'], detail=False, permission_classes=[], authentication_classes=[])
    def register(self, request, *args, **kwargs):
        is_missing, field = missing_fields(request.data, 'password', 'email', 'name')

        if is_missing:
            return Response({"message": f"campo '{field}' é obrigatório."}, status=status.HTTP_400_BAD_REQUEST)

        if User.objects.filter(email=request.data['email']).exists():
            return Response({"message": f"email '{request.data['email']}' já existe."}, status=status.HTTP_409_CONFLICT)

        telegram = request.data.get('telegram', None)
        if telegram:
            if User.objects.filter(telegram=request.data['telegram']).exists():
                return Response({"message": f"telegram @{request.data['telegram']} já existe."},
                                status=status.HTTP_409_CONFLICT)

        res = self.create(request, *args, **kwargs)
        response = dict(user=res.data, message='Usuário criado com sucesso!')

        return Response(response, res.status_code)


def missing_fields(data, *fields):
    for field in fields:
        if not data.get(field, None):
            return True, field
    return False, None
