import random

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from apps.accounts.api.serializers import UserSerializer, UserDetailSerializer


def detail_url(view_name, _id=None, action=None):
    """Return detail of specified url"""
    if action:
        if _id:
            return reverse(f"{view_name}-{action}", args=[_id])
        return reverse(f"{view_name}-{action}")
    return reverse(f'{view_name}-detail', args=[_id])


REGISTER_USER_URL = detail_url('accounts', action='register')
LOGIN_URL = detail_url('accounts', action='login')
USERS_ACCOUNTS_URL = detail_url('accounts', action='list')


def sample_user(**params):
    return get_user_model().objects.create_user(**params)


class TestEndpointUserAccount(TestCase):
    def setUp(self) -> None:
        self.user = sample_user(email='user@email.com', name='user name', password='passuser')
        self.client = APIClient()
        self.client.force_authenticate(self.user)

    def test_user_account_retrieve(self):
        data = UserDetailSerializer(self.user, many=False).data
        response = self.client.get(detail_url('accounts', self.user.id, 'detail'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, data)

        new_user = sample_user(email='new@email.com', name='new name', password='somepass')
        data = UserSerializer(new_user, many=False).data
        response = self.client.get(detail_url('accounts', new_user.id, 'detail'))

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, data)

    def test_user_update_account(self):
        payload = {"name": 'new user name', 'telegram': "new @", "email": "new@email.com", 'bio': "new bio",
                   'password': 'newpass'}

        self.client.patch(detail_url('accounts', self.user.id, action='detail'), payload)

        self.user.refresh_from_db()
        self.assertEqual(self.user.name, payload['name'])
        self.assertEqual(self.user.telegram, payload['telegram'])
        self.assertEqual(self.user.email, payload['email'])
        self.assertEqual(self.user.bio, payload['bio'])
        self.assertTrue(self.user.check_password(payload['password']))

    def test_user_update_other_account(self):
        payload = {"name": 'new user name', 'telegram': "new @", "email": "new@email.com", 'password': "pass"}
        user = sample_user(**payload)
        response = self.client.patch(detail_url('accounts', user.id, action='detail'), payload)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retrieve_list_users(self):
        sample_user(email='user1@email', name='user 1', password='easy pass')
        sample_user(email='user2@email', name='user 2', password='easy pass')
        sample_user(email='user3@email', name='user 3', password='easy pass')

        response = self.client.get(USERS_ACCOUNTS_URL)
        users = get_user_model().objects.all().exclude(id=self.user.id)

        serializer = UserSerializer(users, many=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_retrieve_users_client_unauthorized(self):
        client = APIClient()
        response = client.get(USERS_ACCOUNTS_URL)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_action_evaluate_user(self):
        other_user = sample_user(email='other@email.com', name='other user', password='some pass')

        evaluations = [random.randint(1, 10) for i in range(4)]
        mean = sum(evaluations) / len(evaluations)

        for evaluation in evaluations:
            self.client.post(detail_url('accounts', other_user.id, 'evaluate'), {"evaluation": evaluation})

        other_user.refresh_from_db()
        self.assertAlmostEqual(other_user.evaluation, mean)


class TestEndpointAccountsRegister(TestCase):
    def setUp(self) -> None:
        self.cliente = APIClient()

    def test_register_success(self):
        data = dict(email='user@email.com', password='this pass', name='user name')
        response = self.cliente.post(REGISTER_USER_URL, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['message'], 'Usuário criado com sucesso!')
        self.assertNotEqual(None, response.data['user']['token'])

    def test_register_no_email(self):
        data = dict(password='this pass', name='user name')
        response = self.cliente.post(REGISTER_USER_URL, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], "campo 'email' é obrigatório.")

    def test_register_no_name(self):
        data = dict(password='this pass', email='user@email.com')
        response = self.cliente.post(REGISTER_USER_URL, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], "campo 'name' é obrigatório.")

    def test_register_no_password(self):
        data = dict(name='use name', email='user@email.com')
        response = self.cliente.post(REGISTER_USER_URL, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], "campo 'password' é obrigatório.")

    def test_register_existing_email(self):
        data = dict(email='user@email.com', password='this pass', name='user name')
        response = self.cliente.post(REGISTER_USER_URL, data=data)
        if response.status_code == status.HTTP_201_CREATED:
            new_data = dict(email='user@email.com', password='this pass', name='user name')
            new_response = self.cliente.post(REGISTER_USER_URL, data=new_data)
            self.assertEqual(new_response.status_code, status.HTTP_409_CONFLICT)
            self.assertEqual(new_response.data['message'], f"email '{new_data['email']}' já existe.")

    def test_register_existing_telegram(self):
        data = dict(email='user@email.com', password='this pass', name='user name', telegram='user telegram')
        response = self.cliente.post(REGISTER_USER_URL, data=data)
        if response.status_code == status.HTTP_201_CREATED:
            new_data = dict(email='other@email.com', password='this pass', name='user name', telegram='user telegram')
            new_response = self.cliente.post(REGISTER_USER_URL, data=new_data)
            self.assertEqual(new_response.status_code, status.HTTP_409_CONFLICT)
            self.assertEqual(new_response.data['message'], f"telegram @{new_data['telegram']} já existe.")


class TestEndpointAccountsLogin(TestCase):
    def setUp(self) -> None:
        self.cliente = APIClient()

    def test_login_success(self):
        data = dict(email='user@email.com', password='this pass', name='user name')
        response_register = self.cliente.post(REGISTER_USER_URL, data=data)
        token = response_register.data['user']['token']

        response = self.cliente.post(LOGIN_URL, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['message'], "Login efetuado com sucesso")
        self.assertEqual(token, response.data['user']['token'])

    def test_login_fail(self):
        data = dict(email="this@email.com", password='this pass')
        response = self.cliente.post(LOGIN_URL, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], "Senha ou email inválido")

    def test_login_no_email(self):
        data = dict(password='this pass')
        response = self.cliente.post(LOGIN_URL, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], "campo 'email' é obrigatório.")

    def test_login_no_password(self):
        data = dict(email='email@email.com')
        response = self.cliente.post(LOGIN_URL, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data['message'], "campo 'password' é obrigatório.")
