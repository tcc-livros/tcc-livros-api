from django.contrib import admin

from .models import BookClub

admin.site.register(BookClub)
