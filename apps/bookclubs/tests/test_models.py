from django.test import TestCase

from apps.accounts.models import User
from apps.bookclubs.models import BookClub


class TestBookClubModel(TestCase):
    def setUp(self) -> None:
        self.user = User.objects.create_user('uesr@email.com', 'his name', 'his_pass')
        self.bookclub_attrs = dict(
            name='book club name',
            telegram='telegram of the book',
            description='his club is about some thing',
            admin=self.user,
            theme='what is this club is about'
        )
        self.bookclub = BookClub.objects.create(**self.bookclub_attrs)

    def test_str_representation_of_bookclub(self):
        self.assertEqual(str(self.bookclub), self.bookclub_attrs['name'], '__str__ of bookclub must be its name')

    def test_deletion_of_admin_on_cascade(self):
        self.user.delete()
        book = BookClub.objects.filter(id=1).first()
        self.assertIsNone(book, 'deletion of user must delete bookclub as well')
