from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from ..api.serializers import BookClubSerializer
from ..models import BookClub

BOOKCLUBS_URL = reverse('bookclubs-list')


def sample_bookclub(admin, name='Book club', telegram='@telegram',
                    description='a book club about books', theme='read is important'):
    return BookClub.objects.create(name=name, telegram=telegram, description=description,
                                   admin=admin, theme=theme)


class TestEndpointBookClubs(TestCase):

    def setUp(self) -> None:
        self.data = dict(email='user@email.com', password='this pass', name='user name')
        self.user = get_user_model().objects.create_user(**self.data)
        self.client = APIClient()
        self.client.force_authenticate(self.user)
        self.bookclub_data = dict(name='this bookclub', telegram='bookclubs telegram', description='a nice description',
                                  theme='a cool theme')

    def test_create_bookclub_success(self):
        response = self.client.post(BOOKCLUBS_URL, data=self.bookclub_data)
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_create_bookclub_no_name(self):
        self.bookclub_data.pop('name')
        response = self.client.post(BOOKCLUBS_URL, data=self.bookclub_data)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_create_bookclub_no_telegram(self):
        self.bookclub_data.pop('telegram')
        response = self.client.post(BOOKCLUBS_URL, data=self.bookclub_data)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_create_bookclub_no_description(self):
        self.bookclub_data.pop('description')
        response = self.client.post(BOOKCLUBS_URL, data=self.bookclub_data)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_create_bookclub_no_theme(self):
        self.bookclub_data.pop('theme')
        response = self.client.post(BOOKCLUBS_URL, data=self.bookclub_data)
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_create_bookclub_no_authentication(self):
        new_client = APIClient()
        response = new_client.post(BOOKCLUBS_URL, data=self.bookclub_data)
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)

    def test_get_all_bookclubs(self):
        sample_bookclub(admin=self.user)
        sample_bookclub(admin=self.user)
        sample_bookclub(admin=self.user)

        new_client = APIClient()
        user = get_user_model().objects.create_user(email='newuser@email.com', password='this pass', name='user name')
        new_client.force_authenticate(user)

        serializer = BookClubSerializer(BookClub.objects.all(), many=True)

        response = new_client.get(BOOKCLUBS_URL)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_update_bookclub_by_admin(self):
        book_club = sample_bookclub(admin=self.user)
        payload = dict(name='new name', telegram='@new telegram', description="new description", theme='other theme')

        response = self.client.patch(reverse('bookclubs-detail', args=[book_club.id]), payload)

        book_club.refresh_from_db()
        serializer = BookClubSerializer(book_club)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, serializer.data)

    def test_retrieve_bookclub(self):
        book_club = sample_bookclub(self.user)
        serializer = BookClubSerializer(book_club)

        response = self.client.get(reverse('bookclubs-detail', args=[book_club.id]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(serializer.data, response.data)

        new_user = get_user_model().objects.create_user(email='new@email.com', name='new user', password='some pass')
        client = APIClient()
        client.force_authenticate(new_user)

        response = client.get(reverse('bookclubs-detail', args=[book_club.id]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(serializer.data, response.data)

    def test_update_bookclub_by_not_admin(self):
        new_user = get_user_model().objects.create_user(email='new@email.com', name='new user', password='some pass')
        client = APIClient()
        client.force_authenticate(new_user)
        book_club = sample_bookclub(self.user)
        payload = dict(name='new name', telegram='@new telegram', description="new description", theme='other theme')

        response = client.patch(reverse('bookclubs-detail', args=[book_club.id]), payload)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
